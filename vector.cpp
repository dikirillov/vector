#include "vector.h"

Vector::Iterator::Iterator() {
    it_ = nullptr;
}

Vector::Iterator::Iterator(ValueType* pointer) {
    it_ = pointer;
}

Vector::ValueType& Vector::Iterator::operator*() const {
    return *it_;
}

Vector::Iterator& Vector::Iterator::operator=(Vector::Iterator other) {
    it_ = other.it_;
    return *this;
}

Vector::Iterator& Vector::Iterator::operator++() {
    ++it_;
    return *this;
}

Vector::ValueType* Vector::Iterator::operator->() const {
    return it_;
}

Vector::Iterator Vector::Iterator::operator++(int) {
    Vector::Iterator ans(it_);
    ++it_;
    return ans;
}

Vector::Iterator& Vector::Iterator::operator--() {
    --it_;
    return *this;
}

Vector::Iterator Vector::Iterator::operator--(int) {
    Vector::Iterator ans(it_);
    --it_;
    return ans;
}

Vector::Iterator Vector::Iterator::operator+(Vector::DifferenceType shift) {
    return Vector::Iterator(it_ + shift);
}

Vector::DifferenceType Vector::Iterator::operator-(Vector::Iterator other) {
    return it_ - other.it_;
}

Vector::Iterator& Vector::Iterator::operator+=(Vector::DifferenceType shift) {
    it_ += shift;
    return *this;
}

Vector::Iterator& Vector::Iterator::operator-=(Vector::DifferenceType shift) {
    it_ -= shift;
    return *this;
}

bool Vector::Iterator::operator==(const Vector::Iterator& other) const {
    return it_ == other.it_;
}

bool Vector::Iterator::operator!=(const Vector::Iterator& other) const {
    return it_ != other.it_;
}

std::strong_ordering Vector::operator<=>(const Vector& other) const {
    for (size_t i = 0; i < std::min(size_, other.size_); ++i) {
        if (numbers_[i] != other.numbers_[i]) {
            return numbers_[i] <=> other.numbers_[i];
        }
    }
    return size_ <=> other.size_;
}

Vector::Vector() {
    numbers_ = new int[0];
}

Vector::Vector(size_t size) {
    size_ = size;
    capacity_ = size;
    numbers_ = new int[size]();
}

Vector::Vector(std::initializer_list<ValueType> list) {
    size_t current_size = list.size();
    numbers_ = new int[current_size];
    size_ = current_size;
    capacity_ = current_size;
    auto iter = list.begin();
    for (size_t i = 0; i < current_size; ++i) {
        numbers_[i] = *iter;
        ++iter;
    }
}

Vector::Vector(const Vector& other) {
    numbers_ = other.numbers_;
    size_ = other.size_;
    capacity_ = other.capacity_;
}

Vector& Vector::operator=(const Vector& other) {
    delete[] numbers_;
    size_ = other.size_;
    capacity_ = other.capacity_;
    numbers_ = other.numbers_;
    return *this;
}

Vector::~Vector() {
    delete[] numbers_;
    size_ = 0;
    capacity_ = 0;
}

Vector::SizeType Vector::Size() const {
    return size_;
}

Vector::SizeType Vector::Capacity() const {
    return capacity_;
}

const Vector::ValueType* Vector::Data() const {
    return numbers_;
}

Vector::ValueType& Vector::operator[](size_t position) {
    if (position >= size_) {
        throw std::out_of_range("index out of range in Vector::operator[]");
    }
    return numbers_[position];
}

Vector::ValueType Vector::operator[](size_t position) const {
    if (position >= size_) {
        throw std::out_of_range("index out of range in Vector::operator[]");
    }
    return numbers_[position];
}

bool Vector::operator==(const Vector& other) const {
    if (size_ != other.size_) {
        return false;
    }
    for (size_t i = 0; i < size_; ++i) {
        if (numbers_[i] != other.numbers_[i]) {
            return false;
        }
    }
    return true;
}

bool Vector::operator!=(const Vector& other) const {
    return !(*this == other);
}

void Vector::Reserve(SizeType new_capacity) {
    if (new_capacity > capacity_) {
        auto* tmp = new int[new_capacity];
        for (size_t i = 0; i < size_; ++i) {
            tmp[i] = numbers_[i];
        }
        delete[] numbers_;
        numbers_ = tmp;
        capacity_ = new_capacity;
    }
}

void Vector::Clear() {
    size_ = 0;
}

void Vector::PushBack(const ValueType& new_element) {
    if (size_ < capacity_) {
        numbers_[size_] = new_element;
        ++size_;
    } else {
        if (capacity_ == 0) {
            auto* tmp = new int[1];
            tmp[0] = new_element;
            delete[] numbers_;
            numbers_ = tmp;
            size_ = 1;
            capacity_ = 1;
        } else {
            auto* tmp = new int[2 * capacity_];
            for (size_t i = 0; i < size_; ++i) {
                tmp[i] = numbers_[i];
            }
            tmp[size_] = new_element;
            delete[] numbers_;
            numbers_ = tmp;
            ++size_;
            capacity_ *= 2;
        }
    }
}

void Vector::PopBack() {
    if (size_ == 0) {
        throw std::out_of_range("index out of range in Vector::PopBack()");
    }
    --size_;
}

void Vector::Swap(Vector& other) {
    std::swap(numbers_, other.numbers_);
    std::swap(size_, other.size_);
    std::swap(capacity_, other.capacity_);
}

Vector::Iterator Vector::Begin() {
    return Iterator(numbers_);
}

Vector::Iterator Vector::End() {
    return Iterator(numbers_ + size_);
}

Vector::Iterator Vector::begin() {
    return this->Begin();
}

Vector::Iterator Vector::end() {
    return this->End();
}
